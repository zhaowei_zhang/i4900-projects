/* kem-enc.c

 * simple encryption utility providing CCA2 security.

 * based on the KEM/DEM hybrid model. */



#include <stdio.h>

#include <stdlib.h>

#include <getopt.h>

#include <string.h>

#include <fcntl.h>

#include <openssl/sha.h>

#include <errno.h>

#include "ske.h"

#include "rsa.h"

#include "prf.h"

size_t nBits = 1024;

static const char* no_usage=

"Usage:\n"

"Encrypt or decrypt data.\n\n"

"   -i,--in     FILE   read input from FILE.\n"

"   -o,--out    FILE   write output to FILE.\n"

"   -k,--key    FILE   the key.\n"

"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"

"   -e,--enc           encrypt (this is the default action).\n"

"   -d,--dec           decrypt.\n"

"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"

"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"

"                      RSA key; the symmetric key will always be 256 bits).\n"

"                      Defaults to %lu.\n"

"   --help             show this message and exit.\n";

static const char* usage =

"Usage: %s [OPTIONS]...\n"

"Encrypt or decrypt data.\n\n"

"   -i,--in     FILE   read input from FILE.\n"

"   -o,--out    FILE   write output to FILE.\n"

"   -k,--key    FILE   the key.\n"

"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"

"   -e,--enc           encrypt (this is the default action).\n"

"   -d,--dec           decrypt.\n"

"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"

"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"

"                      RSA key; the symmetric key will always be 256 bits).\n"

"                      Defaults to %lu.\n"

"   --help             show this message and exit.\n";



#define FNLEN 255



enum modes {

	ENC,

	DEC,

	GEN

};



/* Let SK denote the symmetric key.  Then to format ciphertext, we

 * simply concatenate:

 * +------------+----------------+

 * | RSA-KEM(X) | SKE ciphertext |

 * +------------+----------------+

 * NOTE: reading such a file is only useful if you have the key,

 * and from the key you can infer the length of the RSA ciphertext.

 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the

 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",

 * so we will use different hash functions:  H := SHA256, while

 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c

 * (see KDF_KEY).

 * */



#define HASHLEN 32 /* for sha256 */



int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)

{

	/* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;

	 * encrypt fnIn with SK; concatenate encapsulation and cihpertext;

	 * write to fnOut. */

	size_t key_len = rsa_numBytesN(K);

	unsigned char* x = malloc(key_len);

	unsigned char* ct = malloc(key_len);

	x[key_len-1] = 0;

	randBytes(x,key_len-1);

	size_t ct_len = rsa_encrypt(ct, x, key_len, K);

	unsigned char hash_x[HASHLEN];

	SHA256(x, key_len, hash_x);

	size_t encap_len = key_len + HASHLEN;

	unsigned char* encap = malloc(encap_len);

	memcpy(encap,ct,key_len);

	memcpy(encap + key_len,hash_x,HASHLEN);

	

	FILE *fp = fopen(fnOut, "w+");

	fwrite(encap, encap_len, 1, fp);

	fclose(fp);

	

	SKE_KEY skeK;

	ske_keyGen(&skeK,x,key_len);

	ske_encrypt_file(fnOut,fnIn,&skeK,NULL,encap_len);

	

	free(x); free(ct); free(encap);

	return 0;

}



/* NOTE: make sure you check the decapsulation is valid before continuing */

int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)

{

	/* TODO: write this. */

	/* step 1: recover the symmetric key */

	/* step 2: check decapsulation */

	/* step 3: derive key from ephemKey and decrypt data. */

	size_t key_len = rsa_numBytesN(K);

	unsigned char* rsa_dt = malloc(key_len);

	unsigned char* rsa_ct = malloc(key_len);

	unsigned char* encap = malloc(key_len + HASHLEN);

	unsigned char c1[HASHLEN + 1];

	c1[HASHLEN] = 0;

	

	FILE *fp = fopen(fnIn, "r");

	fread(encap, key_len + HASHLEN, 1, fp);

	fclose(fp);

	

	memcpy(rsa_ct, encap, key_len);

	rsa_decrypt(rsa_dt,rsa_ct,key_len,K);

	memcpy(c1, encap + key_len, HASHLEN);

	unsigned char hash_x[HASHLEN + 1];

	hash_x[HASHLEN] = 0;

	SHA256(rsa_dt, key_len, hash_x);



	if(strcmp(c1, hash_x))

		return -1;

	SKE_KEY skeK;

	ske_keyGen(&skeK,rsa_dt,key_len);

	ske_decrypt_file(fnOut,fnIn,&skeK,key_len + HASHLEN);

	

	free(rsa_dt); free(rsa_ct);

	return 0;

}



int main(int argc, char *argv[]) {

	/* define long options */

	static struct option long_opts[] = {

		{"in",      required_argument, 0, 'i'},

		{"out",     required_argument, 0, 'o'},

		{"key",     required_argument, 0, 'k'},

		{"rand",    required_argument, 0, 'r'},

		{"gen",     required_argument, 0, 'g'},

		{"bits",    required_argument, 0, 'b'},

		{"enc",     no_argument,       0, 'e'},

		{"dec",     no_argument,       0, 'd'},

		{"help",    no_argument,       0, 'h'},

		{0,0,0,0}

	};

         if(argc<=2)

          {

            printf(no_usage,nBits);

            return 0;

          }

	/* process options: */

	RSA_KEY K;

	FILE *fp;

	char c;

	int opt_index = 0;

	char fnRnd[FNLEN+1] = "/dev/urandom";

	fnRnd[FNLEN] = 0;

	char fnIn[FNLEN+1];

	char fnOut[FNLEN+1];

	char fnKey[FNLEN+1];

	memset(fnIn,0,FNLEN+1);

	memset(fnOut,0,FNLEN+1);

	memset(fnKey,0,FNLEN+1);

	int mode = ENC;

	// size_t nBits = 2048;

	

	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {

		switch (c) {

			case 'h':

				printf(usage,argv[0],nBits);

				return 0;

			case 'i':

				strncpy(fnIn,optarg,FNLEN);

				break;

			case 'o':

				strncpy(fnOut,optarg,FNLEN);

				break;

			case 'k':

				strncpy(fnKey,optarg,FNLEN);

				break;

			case 'r':

				strncpy(fnRnd,optarg,FNLEN);

				break;

			case 'e':

				mode = ENC;

				break;

			case 'd':

				mode = DEC;

				break;

			case 'g':

				mode = GEN;

				strncpy(fnOut,optarg,FNLEN);

				break;

			case 'b':

				nBits = atol(optarg);

				break;

			case '?':

				printf(usage,argv[0],nBits);

				return 1;

		}

	}



	/* TODO: finish this off.  Be sure to erase sensitive data

	 * like private keys when you're done with them (see the

	 * rsa_shredKey function). */

	switch (mode) {

		case ENC:

			fp = fopen (fnKey, "r");

                        if(fp==NULL)

                        {

                         printf("\n%s open file failed:[%s]\n",fnKey,strerror(errno));

                         exit(-1);

                        }

			rsa_readPublic(fp, &K);

			close(fp);

			kem_encrypt(fnOut, fnIn, &K);

			rsa_shredKey(&K);

			break;

		case DEC:

			fp = fopen (fnKey, "r");

			rsa_readPrivate(fp, &K);

			close(fp);

			kem_decrypt(fnOut, fnIn, &K);

			rsa_shredKey(&K);

			break;

		case GEN:

			rsa_keyGen(nBits, &K);

			fp = fopen (fnOut, "w+");

			rsa_writePrivate(fp, &K);

			close(fp);

			snprintf(fnIn, FNLEN, "%s.pub", fnOut);

			fp = fopen (fnIn, "w+");

			rsa_writePublic(fp, &K);

			close(fp);

			rsa_shredKey(&K);

		default:

			return 1;

	}



	return 0;

}