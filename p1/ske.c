#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
	 if(entropy == NULL)
	 {
		 randBytes(K->hmacKey,KLEN_SKE);
		 randBytes(K->aesKey,KLEN_SKE);
	 }
	 else{
		 unsigned char Key[KLEN_SKE * 2];
		 HMAC(EVP_sha512(),KDF_KEY,HM_LEN,entropy,entLen,Key,NULL);
		 memcpy(K->hmacKey, Key, KLEN_SKE);
		 memcpy(K->aesKey, Key + KLEN_SKE, KLEN_SKE);
		}
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */
	if(!IV) 
	{
		size_t IV_len;
		IV_len = 16;
		IV = malloc(IV_len);
		FILE* frand = fopen("/dev/urandom", "rb");
		fread(IV,1,IV_len,frand);
		fclose(frand);		
	}
	 unsigned char ct[len];
	 unsigned char ct_y[len+16];
	 unsigned char y[len];
	 /*encrypt*/
	memset(ct,0,len);
	memset(y,0,len);
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	int nWritten;
	if (1!=EVP_EncryptUpdate(ctx,ct,&nWritten, inBuf,len))
		ERR_print_errors_fp(stderr);
	EVP_CIPHER_CTX_free(ctx);
	memcpy(outBuf, IV, 16);
	memcpy(outBuf + 16, ct, nWritten); 
	memcpy(ct_y, IV, 16);
	memcpy(&ct_y[16], ct, nWritten);
	
	/*HMAC*/
	HMAC(EVP_sha256(),K->hmacKey,KLEN_SKE,ct_y,nWritten + 16,y,NULL);
	memcpy(outBuf + 16 + nWritten, y, HM_LEN);
	size_t ctLen = nWritten + HM_LEN + 16; 
	return ctLen; /* TODO: should return number of bytes written, which
	             hopefully matches ske_getOutputLen(...). */
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	/* TODO: write this.  Hint: mmap. */
	int fd;
	unsigned char *data;
	struct stat s;
	int size;
	fd = open(fnin, O_RDONLY);	
	int status = fstat (fd, &s);
	size = s.st_size;
	data = (unsigned char*) mmap(0, size, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);	
	size_t ctLen = ske_getOutputLen(size);
	unsigned char* ct = malloc(ctLen);
	ske_encrypt(ct,data,size,K,IV);	
	int out_file;
	if (!offset_out)
		out_file = open(fnout, O_RDWR | O_CREAT |O_TRUNC, 00666);
	else
		out_file = open(fnout, O_RDWR | O_CREAT, 00666);
	lseek(out_file, offset_out, SEEK_SET);
	write(out_file, ct, ctLen);
	status = fstat (out_file, &s);
	s.st_size += ctLen;
	close(out_file);
	
	free(ct);
	return 0;
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */
	if (inBuf == NULL)
		return -1;
	unsigned char iv[16];
	unsigned char iv_x[len-HM_LEN];
	unsigned char y[HM_LEN + 1];
	unsigned char h_x[HM_LEN + 1];
	memset(y,0,HM_LEN + 1);
	memset(h_x,0,HM_LEN + 1);
	unsigned char ct[len];
	int ctlen = len - 16 - HM_LEN; // len is inBuf length
	memcpy(iv, inBuf, 16);
	memcpy(ct, inBuf + 16, ctlen ); 
	memcpy(y, inBuf + 16 + ctlen, HM_LEN);
	memcpy(iv_x, inBuf, len-HM_LEN);
	HMAC(EVP_sha256(),K->hmacKey,KLEN_SKE,iv_x,ctlen + 16,h_x,NULL);
	if (strcmp(y, h_x))
		return -1;

	int nWritten = 0;
	unsigned char pt[ctlen];
	memset(pt,0,ctlen);
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,iv))
		ERR_print_errors_fp(stderr);
	if (1!=EVP_DecryptUpdate(ctx,pt,&nWritten,ct,ctlen))
		ERR_print_errors_fp(stderr);

	memcpy(outBuf , pt, nWritten);
	return nWritten;
	return 0;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
	/* TODO: write this. */
	int fd;
	unsigned char *data;
	struct stat s;
	size_t size;
	fd = open(fnin, O_RDONLY);
	int status = fstat (fd, &s);
	size = s.st_size;
	data = (unsigned char*) mmap(0, size, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);
	size_t ct_len = size - offset_in;
	unsigned char* dt = malloc(ct_len);
	size_t dt_len = ske_decrypt(dt,data+ offset_in ,ct_len,K);
	int out_file = open(fnout, O_WRONLY | O_CREAT |O_TRUNC, 00666);
	write(out_file, dt, dt_len);
	close(out_file);
	free(dt);
	return 0;
}
